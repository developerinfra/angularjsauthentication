# Token Based Authentication using ASP.NET Web API 2, OWIN, and Identity #

Code based on the online tutorial, "Token Based Authentication using ASP.NET Web API 2, Owin, and Identity" by Taiseer Joudeh: http://bitoftech.net/2014/06/01/token-based-authentication-asp-net-web-api-2-owin-asp-net-identity/

In this tutorial we’ll build SPA using AngularJS for the front-end, and ASP.NET Web API 2, OWIN middleware, and ASP.NET Identity for the back-end.